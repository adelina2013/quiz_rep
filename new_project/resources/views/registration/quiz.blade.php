<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{url('/css/quiz.css')}}">

<div class="container">
    <div class="began">
        <div>
            <p>Եթե պատրաստ եք,սկսե՞նք խաղը ․․․</p>
            <button id ="dSubmit"  type ="button" class="btn">Սկսել խաղը</button>
        </div>
        <input type="hidden" value="<?php if(!empty($session)) echo $session->id; ?>" id="ses" >
            {{--            @csrf--}}
            <ul class ="test-list list-unstyled" style="display:none">
                <?php foreach($questions as $key=>$question) { ?>
                <li class="test-list-item">
                    <div class="question" data-question="<?php echo $question->id; ?>">
                        <span class="test-number"><?php echo $key +1; ?></span>
                        <p><?php echo $question->title; ?></p>
                    </div>
                    <div class ="btn-block skill-test-answers" data-question-id = "<?php echo $question->id;?>">
                        <div data-toggle ="buttons">
                            <?php
                            foreach($answers as $key_answer=>$answer) {
                                if($question->id == $answer->question_id) { ?>
                            <label class="btn-default answer-button"  data-answer="<?php echo $answer->id; ?>">
                                <input type="radio" name="number_<?php echo $key; ?>" value ="<?php echo $answer->id; ?>>" data> <?php echo $answer->title; ?>
                            </label>
                            <?php
                                }
                            } ?>
                        </div>

                    </div>
                </li>
                <?php } ?>

            </ul>
        <div class="data-result"></div>
    </div>
    <div>
    </div>

</div>

<div style="width:150px;">
    <p>
        Ցնկանու՞մ եք դուրս գալ
        <a href="logout">Դուրս գալ</a>
    </p>
</div>
<script>
    $("#dSubmit").click( function() {
        $('.test-list').css("display","block");
    });

    var in_ajax = false;
    $('.answer-button').on('click', function(){
        if($(this).hasClass('answered')){
            return false;
        }

        if(!in_ajax) {
            in_ajax = true;

            var _this = $(this);
            var sessionid = $('#ses').val();
            var questionId = $(this).closest('.skill-test-answers').data('question-id');
            var answerId = parseInt($(this).find('input').val());

            $.ajax({
                type: 'POST',
                url:  'quiz/check',
                data: {
                    sessionId:sessionid,
                    questionId:questionId,
                    answerId:answerId},
                success: function (response) {
                    in_ajax = false;

                    if(response.status == 1) {

                        if(response.is_right == 1) {
                            //alert($(this));

                            _this.addClass('right-answer');
                        } else {
                            _this.addClass('wrong-answer');
                            _this.parent().find('[data-answer="' + response.right_answer_id + '"]').addClass('right-answer');
                        }


                        _this.parent().find('label').addClass('answered');
                        if(response.count == 5) {
                            //alert($(this));
                            $('.data-result').text("Դուք վաստակել եք  " + response.right_answer_count + " միավոր") ;
                            $('.data-result').css("color","green") ;
                        }

                    } else {
                        return false;
                    }

                }
            })
        }
    });
</script>
