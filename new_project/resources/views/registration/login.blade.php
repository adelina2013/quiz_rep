
<!DOCTYPE html>
<html>
    <head>
        <title>Registration system PHP and MySQL</title>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{{url('/css/custom.css')}}">
</head>
    <body>
        <form id="logForm">
            <div class="header">
                <h2>Մուտք գործել</h2>
            </div>
{{--            @csrf--}}
            <div class ="display_none"><p></p></div>
            <div class="input-group">
                <label>Էլ․ հասցե</label>
                <input type="email" name="email" value="">
            </div>
            <div class="input-group">
                <label>Գաղտնաբառ</label>
                <input type="password" name="password">
            </div>
            <div class="input-group">
                <button type="button" class="btn dSave" >Մուտք գործել</button>
            </div>
        </form>
    </body>
</html>
<script>
    $('.dSave').click(function(){


        var password = $('[name="password"]');

        var email = $('[name="email"]');
        var is_ok = true;

        $('div.display_none p').text('');

        if(password.val() == "" || email.val() == ""){
            is_ok = false;
            $('div.display_none p').text('Լրացրեք դաշտերը');
            $('div.display_none p').css('color','red');
        }
        if(is_ok) {
            $.ajax({
                type: 'post',
                url: 'login',
                data: $('#logForm').serialize(),
                success: function (response) {
                    if (response.status == 1) {
                        $('div.display_none p').text('');
                        window.location.href = "/quiz";
                    }
                    else{
                        $('div.display_none p').css('color','red');
                        $('div.display_none p').text(response.message);
                    }
                }
            })
        }
    });
</script>
