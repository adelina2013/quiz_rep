
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

        <title>Registration system PHP and MySQL</title>
        <link rel="stylesheet" type="text/css" href="{{url('/css/custom.css')}}">

    </head>
    <body>

        <form  id="registForm">
            <div class="header">
                <h2>Ռեգիստրացիա</h2>
            </div>
            @csrf
            <div class ="display_none"><p></p></div>
            <div class="input-group">
                <label>Անուն</label>
                <input type="text" name="username" value="">
            </div>
             <div class="input-group">
                <label>Էլ․հասցե</label>
                <input type="email" name="email" value="">
             </div>
             <div class="input-group">
                <label>Գաղտնաբառ</label>
                <input type="password" name="password">
            </div>
            <div class="input-group">
                <label>Հաստատել գաղտնաբառը</label>
                <input type="password" name="password_2">
            </div>
            <div class="input-group">
                <button type="button" class="btn saveB">Ռեգիստրացիա</button>
             </div>
             <p>
            Ցանկանու՞մ եք մուտք գործել
                 <a href="login">Մուտք գործել</a>
             </p>
    </form>
    </body>
</html>
<script>
    $('.saveB').click(function(){

        var username = $('[name="username"]');
        var password = $('[name="password"]');
        var rep_password = $('[name="password_2"]');
        var email = $('[name="email"]');
        var is_ok = true;

        $('div.display_none p').text('');

        if(rep_password.val() == "" || password.val() == "" || username.val() == "" || email.val() == ""){
            is_ok = false;
            $('div.display_none p').text('Լրացնել բոլոր դաշտերը');
            $('div.display_none p').css('color','red');
        }
        if(rep_password.val() !== password.val()){
            is_ok = false;
            $('div.display_none p').text('Գաղտնաբառը և կրկնվող գաղտնաբառը չեն համապատասխանում');
            $('div.display_none p').css('color','red');
        }

        if(is_ok) {
            $.ajax({
                type:'post',
                url: 'index',
                data:$('#registForm').serialize(),
                success:function(response){
                    if(response.status == '1'){
                        window.location.href = "/login";
                    } else{
                        $('div.display_none p').text(response.message);
                        $('div.display_none p').css('color','red');
                    }
                }
            })
        }
    });

</script>
