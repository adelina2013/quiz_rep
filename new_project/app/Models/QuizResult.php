<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QuizResult extends Model
{
    use HasFactory;
    protected $table = 'quiz_result';

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function insertData($user, $sessionId, $question_id, $answer_id){
        $values = array('user_id' => $user,'session_id'=>$sessionId, 'question_id' => $question_id, 'answer_id' => $answer_id);
        $insertData = DB::table('quiz_result')->insert($values);
    }

    public static function getUserCorrectAnswers($user_id,$sessionId) {
        $answerResult = DB::table('quiz_result')
            ->select('*')
            ->join("answers", "answers.id", "=", "quiz_result.answer_id")
            ->join("questions", "questions.id", "=", "quiz_result.question_id")
            ->where('quiz_result.session_id',$sessionId)
            ->where('quiz_result.user_id',$user_id)
            ->get();
        return $answerResult;
    }

    public static function deleteUserId($user_id) {
        $rows = DB::table('quiz_result')
            ->where('user_id',$user_id)
            ->delete();
    }



}
