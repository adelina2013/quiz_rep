<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Answers extends Model
{
    use HasFactory;

    protected $table = 'answers';

    protected $fillable = [
        'question_is',
        'title',
        'is_right',
    ];

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getAnswers(){
        $answers = DB::table('answers as a')
            ->select('a.*')
            ->get();
        return $answers;
    }

    /**
     * @param $answerId
     * @return \Illuminate\Support\Collection
     */
    public static function checkAnswer($answerId){
        $answered = DB::table('answers as a')
            ->select('*')
            ->where('a.id',$answerId)
            ->get();
        return $answered;
    }

    /**
     * @param $questionId
     * @return mixed
     */
    public static function correctAnswer($questionId){
        $correctanswer = DB::table('answers as a')
            ->select('a.id as correctId')
            ->join('questions as q', 'a.question_id', '=', 'q.id')
            ->where('a.question_id','=', $questionId)
            ->where('a.is_right','=', 1)
            ->first();
        return $correctanswer->correctId;
    }
}
