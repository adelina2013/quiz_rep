<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Questions extends Model
{
    use HasFactory;

    protected $table = 'questions';

    protected $fillable = [
        'title',
        'point',
    ];

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getQuestions(){
        $questions = DB::table('questions as q')
            ->select('q.*')
            ->inRandomOrder()
            ->limit(5)
            ->get();
        return $questions;
    }

}
