<?php

namespace App\Http\Controllers;

use App\Models\Students;
use App\Models\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;


class UserController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $name = $request['username'];
        $email = $request['email'];
        $password = $request['password'];

        User::create([
        'name'=>$name,
        'email'=>$email,
        'password'=> Hash::make($password),
        ]);
        $credentials = $request->only('email' , 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return response()->json(['status' => 1]);
        } else{
            return response()->json(['status' => 0, 'message' => 'Email |password invalid']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        $email = $request['email'];
        $password = $request['password'];

        $credentials = $request->only('email' , 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return response()->json(['status' => 1]);
        }

           return response()->json(['status' => 0, 'message' =>'Սխալ էլ․հասցե կամ գաղտնաբառ']);
    }
}
