<?php

namespace App\Http\Controllers;

use App\Models\Answers;
use App\Models\cr;
use App\Models\Questions;
use App\Models\QuizResult;
use App\Models\QuizSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class QuizController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        if(Auth::guest()) {
            return Redirect::to('/');
        }
        if(!empty(Auth::user()->id)){
            $session = new QuizSession();
            $session->user_id = Auth::user()->id;
            $session->save();

    }
            $questions = Questions::getQuestions();
            $answers= Answers::getAnswers();
            $shuffle_answers = $answers->shuffle();

            return view("registration/quiz", ['questions' => $questions,'session'=>$session, 'answers' => $shuffle_answers]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(Request $request)
    {
        $id = Auth::user()->id;
        $sessionId = $request['sessionId'];
        $answerId = $request['answerId'];
        $questionId = $request['questionId'];

        $answered = Answers::checkAnswer($answerId);
        $correctAnswerId= Answers::correctAnswer($questionId);

        if(!empty($answerId) || !empty($questionId)){
            QuizResult::insertData($id,$sessionId, $questionId, $answerId);
        }
        $resultAnswers= QuizResult::getUserCorrectAnswers($id,$sessionId);
        $count = 0;
        $point = 0;

        foreach($resultAnswers as $result){
            $count++;
            if($result->is_right == 1){
                $point = $point + $result->point;
            }
        }

        foreach ($answered as $value) {
            $is_right = $value->is_right;
        }

        //$correctAnswerCount = QuizRezult::correctAnswerCount()

        return response()->json(['status'=>1, 'is_right' => $is_right, 'right_answer_id'=>$correctAnswerId,'count'=>$count,'right_answer_count'=>$point]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');

    }
}
