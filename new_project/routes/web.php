<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\RegistrationController;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\QuizController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
Route::get('/', function () {
    return view('registration/create');
});
Route::get('login',function(){
    return view('registration/login');
});



Route::get('quiz',function(){
    return view('registration/quiz');
});


Route::post('index',[UserController::class,'index']);
Route::post('login',[UserController::class,'login']);

Route::get('quiz',[QuizController::class,'index']);
Route::post('quiz/check',[QuizController::class,'check']);

Route::get('logout',[QuizController::class,'logout']);
